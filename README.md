# Docker Webserver
A Docker setup to easily deploy a LAMP webserver with your desired PHP version for each project.
Works in conjunction with the [traefik reverse proxy container](https://bitbucket.org/QuentinBuiteman/docker-traefik.git).

## Setup
Add this project to your git repository as a submodule.
```
git submodule add https://bitbucket.org/QuentinBuiteman/docker-traefik-webserver.git projectname-webserver
```
Navigate to the created folder to create your .env file.
```
cd projectname-webserver
cp .env.example .env
```
Change the env file to your liking. You can set the PHP version and the MySQL version, password and database name.
Make sure to set the NAME to a unique name, like the name of your project. This will correspond to the router name for traefik.

### Apache
If you would like to use Apache, copy the default config file and change the DocumentRoot and ServerName to your liking.
```
cd apache/sites-enabled
cp 000-default.conf.example 000-default.conf
```

## Startup
Navigate into the submodule directory.
```
cd ../../
```

### Start the containers
```
docker-compose up -d
```

### phpMyAdmin
Go to the pma subdomain (for example: pma.yourdomain.com) to go to phpMyAdmin.

## Supported PHP versions
* 5.5
* 5.6
* 7.0
* 7.1
* 7.2
* 7.3
* 7.4
